package com.nbmoody.ponderousapi

import io.ktor.application.*
import io.ktor.auth.*

fun Application.testableMainModule() {
    install(Authentication) {
        basic {
            skipWhen { _ -> true }
        }
        basic("static_auth") {
            skipWhen { _ -> true }
        }
    }
    mainModule()
}
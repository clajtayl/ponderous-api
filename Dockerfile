# Use first stage to build jar
FROM openjdk:8

WORKDIR /app

COPY build.gradle.kts /app/
COPY gradle /app/gradle
COPY gradle.properties /app/
COPY gradlew /app/
COPY settings.gradle.kts /app/

COPY resources /app/resources
COPY src /app/src
COPY test /app/test

RUN /app/gradlew build

# Second stage runs jar
FROM openjdk:8

COPY --from=0 /app/build/libs/ponderous-all.jar .

EXPOSE 8080

CMD java -jar ponderous-all.jar

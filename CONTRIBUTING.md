# Contributing to Ponderous

Straightforwardly:
* Assign yourself and issue from the board (don't be afraid to decompose an issue into sub-issues if you want, just connect the dots for folks)
* Branch off develop and make sure you have the issue referenced in branch name.
* When you're ready, make a merge request and put the issue in the merge request title.
* Never merge your own request. Each request needs my approval before it's eligible.

## If you have ideas
* Feel free to make a ticket, but I retain the right to nix anything, or edit anything, if it doesn't fit the vision of the app.
    * Add the "under review" label to your new tickets. I'll remove it when I've had a chance to take a look at it (and I will!)

Thanks for your work!

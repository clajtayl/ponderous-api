package com.nbmoody.ponderousapi.authentication

import com.auth0.jwt.JWT
import com.auth0.jwt.algorithms.Algorithm
import com.nbmoody.ponderousapi.exception.PonderousException
import com.nbmoody.ponderousapi.util.JWT_SECRET
import java.util.*

class PonderousJWT {
    private val validityInMs = 172800000
    private val hash = Algorithm.HMAC256(getJWTSecret())
    val verifier = JWT.require(hash).build()


    fun sign(resourceId: String): String = JWT
        .create()
        .withClaim("resourceId", resourceId)
        .withExpiresAt(getExpiration())
        .sign(hash)


    private fun getJWTSecret(): String {
        return System.getenv(JWT_SECRET)
            ?: throw PonderousException("JWT_SECRET is not defined in the system environment.")
    }

    private fun getExpiration() = Date(System.currentTimeMillis() + validityInMs)
}
package com.nbmoody.ponderousapi.util

const val DB_URI = "DB_URI"
const val DB_USER = "DB_USER"
const val DB_PASS = "DB_PASS"
const val DB_DRIVER = "org.postgresql.Driver"
const val JWT_SECRET = "JWT_SECRET"
const val CORS_HOST = "CORS_HOST"
package com.nbmoody.ponderousapi.exception

import java.lang.RuntimeException

class PonderousException : RuntimeException {
    constructor(message: String, cause: Throwable) : super(message, cause)
    constructor(message: String) : super(message)
}

class AuthenticationException : RuntimeException()

class AuthorizationException : RuntimeException()

/**
 * This will be thrown when a user tries to create an item that already exists.
 */
class AlreadyExistsException : RuntimeException {
    constructor(message: String, cause: Throwable) : super(message, cause)
    constructor(message: String) : super(message)
}

/**
 * This will be thrown when a user fails to authenticate.
 */
class InvalidCredentialsException : RuntimeException {
    constructor(message: String, cause: Throwable) : super(message, cause)
    constructor(message: String) : super(message)
}

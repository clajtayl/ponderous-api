package com.nbmoody.ponderousapi.repository

import com.nbmoody.ponderousapi.exception.PonderousException
import com.nbmoody.ponderousapi.util.DB_DRIVER
import com.nbmoody.ponderousapi.util.DB_PASS
import com.nbmoody.ponderousapi.util.DB_URI
import com.nbmoody.ponderousapi.util.DB_USER
import com.zaxxer.hikari.HikariConfig
import com.zaxxer.hikari.HikariDataSource
import mu.KotlinLogging
import org.jetbrains.exposed.sql.Database

class DataSource {

    fun initDb() {
        val logger = KotlinLogging.logger {  }

        val db = try {
            Database.connect(hikari())
        } catch (e: Exception) {
            when (e) {
                is SecurityException -> {
                    logger.error("Can't access system environment", e)
                    throw e
                }
                else -> {
                    logger.error("Could not connect to the database.", e)
                    throw PonderousException("Could not connect to the database.", e)
                }
            }
        }
    }

    private fun hikari(): HikariDataSource {
        val dbUri = System.getenv(DB_URI) ?: throw PonderousException("DB_URI not defined in system environment.")
        val dbUser = System.getenv(DB_USER) ?: throw PonderousException("DB_USER not defined in system environment.")
        val dbPass = System.getenv(DB_PASS) ?: throw PonderousException("DB_PASS not defined in system environment.")

        val config = HikariConfig()
        config.driverClassName = DB_DRIVER
        config.jdbcUrl = dbUri
        config.username = dbUser
        config.password = dbPass
        config.validate()
        return HikariDataSource(config)
    }
}
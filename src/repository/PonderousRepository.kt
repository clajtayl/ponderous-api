package com.nbmoody.ponderousapi.repository

import com.nbmoody.ponderousapi.authentication.PonderousJWT
import com.nbmoody.ponderousapi.authentication.hash
import com.nbmoody.ponderousapi.model.User
import com.nbmoody.ponderousapi.model.Users
import com.nbmoody.ponderousapi.model.toUser
import org.jetbrains.exposed.dao.id.EntityID
import org.jetbrains.exposed.sql.Key
import org.jetbrains.exposed.sql.insert
import org.jetbrains.exposed.sql.insertAndGetId
import org.jetbrains.exposed.sql.select
import org.jetbrains.exposed.sql.transactions.transaction
import org.koin.core.KoinComponent
import org.koin.core.inject
import java.util.*

class PonderousRepository : KoinComponent {

    fun createUser(user: User): EntityID<UUID> {

        return transaction {
            Users.insertAndGetId {
                it[email] = user.email
                it[password] = hash(user.password)
            }
        }

    }

    fun findUser(user: User): User? {

        return transaction {
            val query = Users.select {
                Users.email eq user.email
            }

            query.firstOrNull()?.toUser()
        }

    }
}
package com.nbmoody.ponderousapi.injection

import com.nbmoody.ponderousapi.authentication.PonderousJWT
import com.nbmoody.ponderousapi.repository.DataSource
import com.nbmoody.ponderousapi.repository.PonderousRepository
import org.koin.dsl.module

val mainModule = module {
    // KoinDSL for injecting a class with parameters
    // single { (logger : Logger) -> PonderousRepository(logger) }

    single { PonderousJWT() }
    single { DataSource() }
    single { PonderousRepository() }

}

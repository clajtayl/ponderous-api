-- Create the ponderous database.
DROP DATABASE IF EXISTS "Ponderous";
CREATE DATABASE "Ponderous" WITH ENCODING = 'UTF8';

-- Create the ponderous user.
DROP USER IF EXISTS papplication;
DROP USER IF EXISTS readwrite;

CREATE ROLE readwrite WITH
    NOLOGIN
    NOSUPERUSER
    INHERIT
    NOCREATEDB
    NOCREATEROLE
    NOREPLICATION;

GRANT CONNECT ON DATABASE "Ponderous" TO readwrite;

CREATE USER papplication WITH
    LOGIN
    NOSUPERUSER
    INHERIT
    NOCREATEDB
    NOCREATEROLE
    NOREPLICATION
    PASSWORD 'papplication'
    VALID UNTIL 'infinity';

GRANT readwrite TO papplication;

-- Schema
\connect "Ponderous"
CREATE SCHEMA ponderous;

-- Default schema for logins to the ponderous database
ALTER DATABASE "Ponderous" SET search_path TO ponderous;

-- Usage of SCHEMA
GRANT ALL ON SCHEMA ponderous TO readwrite;

-- Set up default access for new tables / sequences
ALTER DEFAULT PRIVILEGES IN SCHEMA ponderous GRANT USAGE ON SEQUENCES TO readwrite;
ALTER DEFAULT PRIVILEGES IN SCHEMA ponderous GRANT SELECT,INSERT,DELETE,UPDATE ON TABLES TO readwrite;

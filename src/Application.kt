package com.nbmoody.ponderousapi

import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.databind.SerializationFeature
import com.nbmoody.ponderousapi.authentication.PonderousJWT
import com.nbmoody.ponderousapi.exception.*
import com.nbmoody.ponderousapi.injection.mainModule
import com.nbmoody.ponderousapi.model.Users
import com.nbmoody.ponderousapi.repository.DataSource
import io.ktor.application.*
import io.ktor.auth.*
import io.ktor.auth.jwt.*
import io.ktor.features.*
import io.ktor.http.*
import io.ktor.jackson.*
import io.ktor.response.*
import io.ktor.routing.*
import org.jetbrains.exposed.sql.SchemaUtils
import org.jetbrains.exposed.sql.transactions.transaction
import org.koin.core.context.startKoin
import org.koin.ktor.ext.inject

fun main(args: Array<String>): Unit = io.ktor.server.netty.EngineMain.main(args)


fun Application.main() {

    startKoin {
        modules(mainModule)
    }

    val dataSource: DataSource by inject()
    dataSource.initDb()

//    setupDBSchema()

    val ponderousJWT: PonderousJWT by inject()

    install(Authentication) {
        jwt {
            verifier(ponderousJWT.verifier)
            validate {
                JWTPrincipal(it.payload)
            }
        }
    }

}

@Suppress("unused") // Referenced in application.conf
@kotlin.jvm.JvmOverloads
fun Application.mainModule(testing: Boolean = false) {


    install(CORS) {
        method(HttpMethod.Options)
        method(HttpMethod.Put)
        method(HttpMethod.Delete)
        method(HttpMethod.Patch)
        header(HttpHeaders.Authorization)
        allowCredentials = true
        anyHost() // @TODO: Don't do this in production if possible. Try to limit it.
    }


    install(ContentNegotiation) {
        jackson {
            enable(SerializationFeature.INDENT_OUTPUT)
            configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
        }
    }

    install(CallLogging)

    install(StatusPages) {
        exception<AuthenticationException> { cause ->
            call.respond(HttpStatusCode.Unauthorized)
        }
        exception<AuthorizationException> { cause ->
            call.respond(HttpStatusCode.Forbidden)
        }
        exception<PonderousException> { cause ->
            call.respond(HttpStatusCode.InternalServerError)
        }
        exception<AlreadyExistsException> { cause ->
            log.info(cause.message)
            call.respond(HttpStatusCode.Conflict, mapOf("message" to cause.message))
        }
        exception<InvalidCredentialsException> { cause ->
            log.info(cause.message)
            call.respond(HttpStatusCode.Unauthorized, mapOf("message" to cause.message))
        }
        exception<NotFoundException> { exception ->
            log.info(exception.message)
            call.respond(HttpStatusCode.NotFound, mapOf("message" to exception.message))
        }
    }

    routing {
        root()
    }

}

fun setupDBSchema() {

    transaction {
        SchemaUtils.drop(Users)
        SchemaUtils.create(Users)
    }

}

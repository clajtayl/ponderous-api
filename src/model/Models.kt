package com.nbmoody.ponderousapi.model

import org.jetbrains.exposed.dao.id.UUIDTable
import org.jetbrains.exposed.sql.Column
import org.jetbrains.exposed.sql.ResultRow
import org.jetbrains.exposed.sql.Table

data class User(
    val email: String,
    val password: String
)

object Users : UUIDTable("user") {
    val email: Column<String> = varchar("email", 255).uniqueIndex()
    val password: Column<String> = varchar("password", 255)
}

fun ResultRow.toUser() = User(
    this[Users.email],
    this[Users.password]
)
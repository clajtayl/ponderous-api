package com.nbmoody.ponderousapi

import com.nbmoody.ponderousapi.authentication.PonderousJWT
import com.nbmoody.ponderousapi.authentication.hash
import com.nbmoody.ponderousapi.exception.AlreadyExistsException
import com.nbmoody.ponderousapi.exception.InvalidCredentialsException
import com.nbmoody.ponderousapi.model.User
import com.nbmoody.ponderousapi.repository.PonderousRepository
import io.ktor.application.call
import io.ktor.features.NotFoundException
import io.ktor.http.ContentType
import io.ktor.request.receive
import io.ktor.response.respond
import io.ktor.response.respondText
import io.ktor.routing.Routing
import io.ktor.routing.get
import io.ktor.routing.post
import org.koin.ktor.ext.inject

fun Routing.root() {

    val ponderousJWT: PonderousJWT by inject()
    val ponderousRepository: PonderousRepository by inject()

    get("/") {
        call.respondText("HELLO WORLD!", contentType = ContentType.Text.Plain)
    }

    get("/json/jackson") {
        call.respond(mapOf("hello" to "world"))
    }

    post("/authenticate") {

        val authenticatePayload = call.receive<User>()

        val userToCheck = ponderousRepository.findUser(authenticatePayload)

        if (userToCheck != null) {
            if (userToCheck.password == hash(authenticatePayload.password)) {
                call.respond(mapOf("token" to ponderousJWT.sign(userToCheck.email)))
            } else {
                throw InvalidCredentialsException("Incorrect username or password.")
            }
        } else {
            throw NotFoundException("User with this email was not found.")
        }

    }

    post("/register") {
        val registerUserPayload = call.receive<User>()

        val existingUser = ponderousRepository.findUser(registerUserPayload)

        if (existingUser == null) {
            val result = ponderousRepository.createUser(registerUserPayload)
            call.respond(mapOf("message" to "User " + result.value + " created."))
        } else {
            throw AlreadyExistsException("A user with this email already exists.")
        }

    }

}



# Ponderous API

This is a KTor application that functions as the back end for the Ponderous app.

## Local Development

#### From the command line
- to build the application with tests run `./gradlew build` from the command line
    - this will generate a jar file located in the `build/libs` directory
- to build and run the application with tests run `./gradlew build run`
  
- These are the environment variables that need to be set in your environment in order to connect to the database

#### With VSCode Remote Docker Dev Container
- Clone the repo on OS filesystem and open with VSCode.  It will detect the `.devcontainer` folder then a prompt will be generated to open in a container.
- VSCode will build docker images, run container and connect the VSCode Window to the dev container.
- Open a terminal window and copy in (or create, if needed), SSH public and private keys applying the correct permissions (chmod 600 to id_rsa on linux for example).

```
docker cp ~/.ssh/id_rsa.pub <container name or id>:/root/.ssh
docker cp ~/.ssh/id_rsa <container name or id>:/root/.ssh
```
- In the `/workspace/code` directory, `git clone` the repo.
- run `./gradlew build`
- run `java -jar build/libs/ponderous-all.jar`

NOTE:  The current config for the dev container does NOT initialize the DB.  This will be automated
soon via Liquibase.  This can be done using the files in the `sql` folder via the command
line in the postgres container or via your favorite DB tool.

#### With IntelliJ
This will give you a couple extra features such as debugging and hot reloading when making changes.

- Create a new Run Configuration using “Application” as a template
- For the main class use the following engine
    - io.ktor.server.netty.EngineMain
- Specify the Module to be used
    - select `ponderous.main` from the available list
- Add the following environment variables to the environment variable section in the run config window
- Save the configuration by giving it a name

#### With Docker
There is a basic Dockerfile in the repository root. Currently it is set up to connect to the dev environment database. It takes a while to build, so if you want to use it regularly it would be worth optimizing. You can build and run it by following these steps:

- Install Docker
- Get the values for the JWT_SECRET environment variables from another team member.
- Build the image with: `docker build -t ponderous .`
- Run the image:

```
  docker run -d -p 8080:8080 \
    -e DB_URI="" \
    -e DB_USER="" \
    -e DB_PASS="" \
    -e JWT_SECRET="" \
    ponderousapi
 ```

- The API should now be accessible at http://localhost:8080

## Create Local PostgreSQL Instance for Dev
Two options for a local instance

#### Docker (preferred, for ease of use)

#### Direct install
www.postgresql.org/download/

Select your system of choice, download and install.

During installation you will be asked to enter a password for your primary user, named 'postgres', keep track of that password.

After installation, you can open psql which is a SQL shell for the command line, and log into PostgresSQL
  - Server [localhost]: 			[enter]
  - Database [postgres}: 			[enter]
  - Port [5432]: 					[enter]
  - Username [postgres]:			[enter]
  - Password for user postgres:	[your password]  (NB the cursor won't advance during the password entry)

(Windows users will have a warning re: 8-bit characters. Can be ignored.)

postgres-# psql \i 'sql script file path'
